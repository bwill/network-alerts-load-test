FROM loadimpact/k6

COPY load_test/policy_violation.js .

ENTRYPOINT [ "k6", "run", "policy_violation.js" ]
