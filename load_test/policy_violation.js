import http from 'k6/http';
import { sleep } from 'k6';

export let options = {
  stages: [
    { duration: '2m', target: 100 },
    { duration: '2m', target: 100 },
    { duration: '2m', target: 200 },
    { duration: '2m', target: 300 },
    { duration: '2m', target: 400 },
    { duration: '2m', target: 0 },
  ],
};

const url = 'http://nginx.cilium.svc.cluster.local';

export default function () {
  http.get(url);
  sleep(1);
}
