# Network Policy Load Tests

This repository contains K6 load tests used to test the performance impact of
Cilium Network Policies on a Kubernetes cluster.

# How to run these tests

Before running these tests you will need:
- [kubectl](https://kubernetes.io/docs/tasks/tools/#kubectl)
- [helm](https://helm.sh/docs/intro/install/)
- [helm-diff plugin](https://github.com/databus23/helm-diff#install)
- [helmfile](https://github.com/roboll/helmfile#installation)
- Optional: [Install k9s](https://github.com/derailed/k9s#installation) to monitor pod resource utilization

Then, run load tests by doing the following:

1. Create a kubernetes cluster on the platform of your choosing and configure kubectl to connect to your cluster.
   You should be able to successfully run `kubectl get pods`.
1. Install Cilium with `helmfile --file helmfile.yaml apply`
1. Create load test resources with `kubectl apply -f load_test/load_test.yaml`
1. Inspect pods with `k9s -n cilium`. A k6 pod should be created and will send requests to the nginx pod, resulting in a policy violation.
1. View k6 logs with `kubectl logs -n cilium -f job/k6`
1. When test is completed, delete resources with `kubectl delete -f load_test/load_test.yaml`
